#include <iostream>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <memory.h>
#include <unistd.h>

#define ROW 6
#define COLUMN 6

#define MIN_RANDOM_VALUE 0.0
#define MAX_RANDOM_VALUE 100.0
#define PORT_SEND  25000
#define ADDRESS_SEND "127.0.0.1"

double fRand(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main() {
    /* нулить не будем потому, что будем заполнять рандомными значениями*/
    double array[ROW][COLUMN];
    /*инициализация рандома*/
    srand(time(NULL));

    int sockSend = 0;
    struct sockaddr_in addressSend;

    addressSend.sin_family = AF_INET;
    addressSend.sin_port = htons(PORT_SEND);
    addressSend.sin_addr.s_addr = htonl(inet_network(ADDRESS_SEND));

    if ((sockSend = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        fprintf(stdout, "Error socket send\n");
        return -1;
    }


    for (;;) {

        fd_set setWrite;
        ssize_t countBytes = 0;
        FD_ZERO(&setWrite);
        FD_SET(sockSend, &setWrite);

        if (select(sockSend + 1, NULL, &setWrite, NULL, NULL) != -1) {
            if (FD_ISSET(sockSend, &setWrite)) {

                /*заполняем массив*/
                for (int i = 0; i < ROW; i++)
                    for (int j = 0; j < COLUMN; j++)
                        array[i][j] = fRand(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE);

                /*печатаем массив*/
                for (int i = 0; i < ROW; i++) {
                    for (int j = 0; j < COLUMN; j++)
                        std::cout << array[i][j] << " ";
                    std::cout << std::endl;
                }


                if ((countBytes = sendto(sockSend, &array[0][0], sizeof(array), 0,
                                         (struct sockaddr *) &addressSend,
                                         sizeof(struct sockaddr_in))) == -1) {
                    perror("Perror:");
                    return -1;
                }
                std::cout << "Send array : " << countBytes << " bytes" << std::endl;
                std::cout << "----------------------------" << std::endl;

            }
        }

        sleep(2);
    }


    return 0;
}